const express = require("express");
const app = express();
const pool = require('./db');

app.use(express.json()) // => req.body

// Routes //

// get all //
app.get('/coba', async(req, res) => {
    try {
        const all_todos = await pool.query("SELECT * FROM todo");
        res.json(all_todos.rows);
    } catch (err) {
        console.error(err.message);
    }
})

// get //
app.get('/coba/:id', async(req, res) => {
    try {
        const { id } = req.params;
        const a = await pool.query("SELECT * FROM todo WHERE todo_id = $1", [id]);
        res.json(a.rows[0]);
    } catch (err) {
        console.error(err.message);
    }
});

// create //
app.post('/coba', async(req, res) => {
    try {
        const { description } = req.body;
        const newTodo = await pool.query("INSERT INTO todo (description) VALUES ($1) RETURNING *", [description]);
        res.json(newTodo);
    } catch (err) {
        console.error(err.message);
    }
});

// update todo //
app.put('/coba:id', async(req, res) => {
    try {
        const { id } = req.params; //WHERE
        const { description } = req.body; //SELECT

        const update = await pool.query("UPDATE todo SET description =$1 WHERE todo_id = $2", [description, id]);
        res.json("todo was updated");
    } catch (err) {
        console.error(err.message);
    }
});

// delete todo //
app.delete("/coba/:id", async(req, res) => {
    try {
        const { id } = req.params;
        const deleteTodo = await pool.query("DELETE FROM todo WHERE todo_id=$1", [id]);
        res.json(deleteTodo);
    } catch (err) {

    }
});

app.listen(8080, () => {
    console.log("Server running...");
});